# Generated by Django 3.2.8 on 2021-11-13 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ScrumyGoalsApi',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('goal_name', models.CharField(max_length=250)),
                ('goal_id', models.IntegerField(default=1)),
                ('created_by', models.CharField(max_length=250)),
            ],
        ),
    ]
