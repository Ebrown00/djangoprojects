from django.shortcuts import render
from .models import ScrumyGoalsApi
from rest_framework import viewsets, serializers
from .serializers import ScrumyGoalsApiSerializer


class ScrumyGoalsViewSet(viewsets.ModelViewSet):
    queryset = ScrumyGoalsApi.objects.all().order_by('goal_name')
    serializer_class = ScrumyGoalsApiSerializer

# Create your views here.
