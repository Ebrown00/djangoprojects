from rest_framework import serializers
from .models import ScrumyGoalsApi


class ScrumyGoalsApiSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ScrumyGoalsApi
        fields = ('goal_name', 'goal_id')
