
=================
My scrumy
=================
-------------------------
My brownosaweescrumy
-------------------------

Introduction
============

My scrumy is a simple Django app. It  is to demonstrate how to create a Django app.
Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "brownosaweescrumy" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'brownosaweescrumy',
    ]

2. Include the brownosaweescrumy URLconf in your project urls.py like this::
    path('brownosaweescrumy/', include('brownosaweescrumy.urls')),

3. Run 'python manage.py migrate' to create the polls models.

4. Start the development server and visit http://127.0.0.1:8000/admin/ to enter (you'll need the admin app enabled).

5. Visit https://127.0.0.1:8000/brownosaweescrumy/ to use the app.