from setuptools import setup

setup(
    name='brownosaweescrumy',
    version='1.0',
    packages=[''],
    url='http://127.0.0.1:8000/',
    license='BSD license',
    author='Esse',
    author_email='brownosawee@gamil.com',
    description='A simple Django app',
    classifiers=[
        'Environment : : Web Environment',
        'Framework : : Django',
        'Intended Audience : : Developers',
        'License : : OST Approved : : BSD license',
        'Operating System : : Windows',
        'Programming Language : : Python',
        'Programming Language : : Python : : 3.6',
        'Programming Language : : Python : : 3.8',
    ]
)
