from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from .models import ScrumyGoals, GoalStatus
from .form import SignupForm, CreateGoalForm, ChangeGoalStatus
from django.contrib.auth.models import User, Group
import random


# def get_grading_parameters(request):
# return HttpResponse("This is a Scrum Application")


# def index(request):
# my_goal = ScrumyGoals.objects.get(goal_name='Learn Django')
# return HttpResponse(my_goal.goal_name)


def move_goal(request, goal_id):
    goal = ScrumyGoals.objects.get(goal_id=goal_id)
    former_status = goal.goal_status.status_name
    form = ChangeGoalStatus()
    current_user = request.user
    if request.method == 'POST':
        form = ChangeGoalStatus(request.POST, instance=goal)
        if form.is_valid():
            if current_user.groups.filter(name='Developer').exists():
                if current_user.username == goal.owner and goal.goal_status.status_name != 'Done Goal':
                    form.save()
                    messages.success(request, f'Your goals has been successfully moved')
                    return redirect('home')
                else:
                    goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                    goalid.save()
                    form = ChangeGoalStatus()
                    messages.warning(request, f'You can only move your own goals!')
                    # context = {
                    #          'form': form,
                    #          'goal_name': goalid,
                    #          'error': 'You can move your own goal but not to the Done column!'
                    # }
                    # return render (request, 'brownosaweescrumy/movegoal.html', context)
            elif current_user.groups.filter(name='Quality Assurance').exists():
                if current_user.username == goal.owner:
                    form.save()
                    messages.success(request, f'Your goals has been successfully moved.')
                    return redirect('home')
                elif former_status == 'Done Goal' or former_status == 'Verify Goal':
                    form.save()
                    messages.success(request, f'Your goals has been successfully moved.')
                    return redirect('home')
                else:
                    goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                    goalid.save()
                    form = ChangeGoalStatus()
                    messages.warning(request, f'You are only permitted to move goals from/to either a Verify or a '
                                              f'Done status if it was not created by you')
            elif current_user.groups.filter(name='Owner').exists():
                if current_user.username == goal.owner:
                    form.save()
                    messages.success(request, f'Your goals has been successfully moved.')
                    return redirect('home')
                else:
                    goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                    goalid.save()
                    form = ChangeGoalStatus()
                    messages.warning(request, f'You cannot move this goal since you did not create it, You Belong to '
                                              f'the group Owner!!!!')
            elif current_user.groups.filter(name='Admin').exists():
                form.save()
                messages.success(request, f'Your goals has been successfully moved.')
                return redirect('home')
            else:
                goalid = ScrumyGoals.objects.get(goal_id=goal_id)
                goalid.save()
                form = ChangeGoalStatus()
                messages.warning(request, f'Your are not permitted to a move a goal, you do not belong to any group!!')
    else:
        form = ChangeGoalStatus()

    return render(request, 'brownosaweescrumy/movegoal.html', {'form': form})

    # try:
    # goal = ScrumyGoals.objects.get(goal_id=goal_id)
    # except Exception as e:

    # return render(request, 'brownosaweescrumy/exception.html',
    # {'error': 'A record with that goal id does not exist'})
    # else:
    # return HttpResponse(goal.goal_name)


def add_goal(request):
    week = GoalStatus.objects.get(status_name='Weekly Goal')
    daily = GoalStatus.objects.get(status_name='Daily Goal')
    goals = [week]
    # user = User.objects.get(username='louis')
    form = CreateGoalForm()
    if request.method == 'POST':
        form = CreateGoalForm(request.POST)
        if form.is_valid():
            g = form.save(commit=False)
            r1 = random.randint(1000, 9999)
            user = form.cleaned_data['user']
            new_user = User.objects.get(username=user)
            g.goal_id = r1
            g.goal_status = week
            g.created_by = new_user
            g.owner = new_user
            g.moved_by = new_user
            g.save()
            return redirect('home')
    else:
        form = CreateGoalForm()
    context = {'form':
                   form}

    return render(request, 'brownosaweescrumy/addgoal.html', context)

    # id = []
    # for t in range(1000, 9999):
    # r1 = random.randint(1000, 9999)
    # if r1 not in id:
    # id.append(r1)
    # scrumy = ScrumyGoals.objects.create(goal_name='Keep Learning Django', goal_id=r1, created_by='Louis',
    # moved_by='Louis',
    # owner='Louis', goal_status=week, user=user)
    # scrumy.save()
    # return HttpResponse(scrumy.goal_name)


# Create your views here.


def home(request, *args, **kwargs):
    user = User.objects.all()
    current_user = request.user
    # user = User.objects.get(username='louis')
    WeeklyGoals = GoalStatus.objects.get(status_name="Weekly Goal")
    related_weekly_goal = WeeklyGoals.scrumygoals_set.all()
    DailyGoals = GoalStatus.objects.get(status_name="Daily Goal")
    related_daily_goal = DailyGoals.scrumygoals_set.all()
    DoneGoal = GoalStatus.objects.get(status_name="Done Goal")
    related_done_goal = DoneGoal.scrumygoals_set.all()
    VerifyGoal = GoalStatus.objects.get(status_name="Verify Goal")
    related_verify_goal = VerifyGoal.scrumygoals_set.all()

    # my_home = ScrumyGoals.objects.filter(goal_name='Keep Learning Django')
    # scrumy_goal = ScrumyGoals.objects.all()
    # output = ', '.join([t.goal_name for t in my_home])
    # dictionary = {'scrumy_goal': ScrumyGoals.objects.filter(goal_name='Learn Django')}
    # return HttpResponse(output)

    dictionary = {'user': user,
                  'related_weekly_goal': related_weekly_goal,
                  'related_daily_goal': related_daily_goal,
                  'related_done_goal': related_done_goal,
                  'related_verify_goal': related_verify_goal,
                  }

    return render(request, "brownosaweescrumy/home.html", dictionary)


# def loginpage(request):
# try:
# user = User.objects.get(username='louis')
# except KeyError:
# return render(request, 'brownosaweescrumy/login.html', {'error_message': 'Invalid Login credentials'})
# else:
# user.save()
# return render(request, 'brownosaweescrumy/login.html', user)

def index(request):
    user = User.objects.get(username='louis')
    # form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data('username')
            real_password = form.cleaned_data('password')
            my_group = Group.objects.get(name='Developer')
            my_group.user_set.add(user)
            user = authenticate(username=username, password=real_password)
            login(request, user)
            messages.success(request, f'Your account has been successfully created, you can now login!')
            return redirect('message')
    else:
        form = SignupForm()

    return render(request, 'brownosaweescrumy/index.html', {'form': form})


def message(request):
    return render(request, 'brownosaweescrumy/message.html')
