from django.db import models

# Create your models here.
from django.contrib.auth.models import User, Group, Permission


class GoalStatus(models.Model):
    status_name = models.CharField(max_length=200)

    def __str__(self):
        return self.status_name


class ScrumyGoals(models.Model):
    goal_name = models.CharField(max_length=200)
    goal_id = models.IntegerField(default=200)
    goal_status = models.ForeignKey('GoalStatus', on_delete=models.PROTECT, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_name', null=True)
    created_by = models.CharField(max_length=200)
    moved_by = models.CharField(max_length=200)
    owner = models.CharField(max_length=200)

    class Meta:
        permissions = [
            ("create_new_Weekly_goal", " Can create a new Weekly Goal for a user"),
            ("change_weekly_goal_to_daily_goal", "Can change Weekly Goal to Daily Goal"),
            ("change_goal_status_to_done_goal", "can change goal status to Done Goal"),
            ("change_goal_status_to_verify_goal", "can change goal status to Verify Goal"),
            ("change_goal_status_to_daily_goal", "Can change goal status to Daily Goal"),
            ("change_goal_status_to_weekly_goal", "can change goal status to Weekly Goal"),

        ]

    def __str__(self):
        return self.goal_name


class ScrumyHistory(models.Model):
    goal = models.ForeignKey('ScrumyGoals', on_delete=models.CASCADE)
    moved_by = models.CharField(max_length=200)
    created_by = models.CharField(max_length=200)
    moved_from = models.CharField(max_length=200)
    moved_to = models.CharField(max_length=200)
    time_of_action = models.DateTimeField('date carried out')

    def __str__(self):
        return self.created_by
