# Generated by Django 3.2.8 on 2021-11-08 00:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('brownosaweescrumy', '0002_auto_20211024_2108'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='scrumygoals',
            options={'permissions': [('create_new_Weekly_goal', ' Can create a new Weekly Goal for a user'), ('change_weekly_goal_to_daily_goal', 'Can change Weekly Goal to Daily Goal'), ('change_goal_status_to_done_goal', 'can change goal status to Done Goal'), ('change_goal_status_to_verify_goal', 'can change goal status to Verify Goal'), ('change_goal_status_to_daily_goal', 'Can change goal status to Daily Goal'), ('change_goal_status_to_weekly_goal', 'can change goal status to Weekly Goal')]},
        ),
    ]
