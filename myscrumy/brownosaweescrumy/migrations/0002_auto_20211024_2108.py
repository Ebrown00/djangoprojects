# Generated by Django 3.2.8 on 2021-10-24 20:08

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('brownosaweescrumy', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrumygoals',
            name='goal_status',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='brownosaweescrumy.goalstatus'),
        ),
        migrations.AlterField(
            model_name='scrumygoals',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_name', to=settings.AUTH_USER_MODEL),
        ),
    ]
