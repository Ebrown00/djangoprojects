from django import forms
from django.contrib.auth.models import User
from .models import ScrumyGoals, GoalStatus, ScrumyHistory


class SignupForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']


class CreateGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name', 'user', 'goal_status']


class ChangeGoalStatus(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_status', 'goal_name']


class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username']
