from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import ChatMessage, Connection

# Create your views here.
'''WebSocket URL: wss://vgcs99t3uj.execute-api.us-east-2.amazonaws.com/test
   Connection URL: https://vgcs99t3uj.execute-api.us-east-2.amazonaws.com/test/@connections'''

@csrf_exempt
def test(request):
    return JsonResponse({'message': 'hello Daud'}, status=200)


@csrf_exempt
def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)


@csrf_exempt
def connect_view(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connect = Connection(connection_id=connection_id)
    connect.save()
    return JsonResponse({'message': 'Connected successfully'}, status=200)


@csrf_exempt
def disconnect_view(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    disconnect = Connection.objects.get(connection_id=connection_id)
    disconnect.delete()
    return JsonResponse({'message': 'Disconnected successfully'}, status=200)